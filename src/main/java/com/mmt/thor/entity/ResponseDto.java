package com.mmt.thor.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDto {
    private String cityCodes;
    private String flightNos;
    private int journeyTime;
}
