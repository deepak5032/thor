package com.mmt.thor.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

//add fields for flight entity 'ATQ_DEL_BLR': {'2057_819': 365}
@Getter
@Setter
@AllArgsConstructor
public class Flight {
    private String flightNo;
    private String toAirport;
    private String fromAirport;
    private String startTime;
    private String endTime;
}
