package com.mmt.thor.rest;

import com.mmt.thor.entity.ResponseDto;
import com.mmt.thor.service.FlightsAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/flights")
public class RestApi {

    @Autowired
    private FlightsAnalyzer flightsAnalyzer;

    @GetMapping("/fastest/{n}/ways/{source}/{destination}")
    public List<List<ResponseDto>> getNFastestWays(@PathVariable int n, @PathVariable String source,
                                                         @PathVariable String destination) {
        return flightsAnalyzer.getNWaysToTravel(n,source,destination);
    }
}
