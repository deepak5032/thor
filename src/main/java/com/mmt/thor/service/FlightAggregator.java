package com.mmt.thor.service;

import com.mmt.thor.entity.Flight;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

@Service
public class FlightAggregator {

    private final String fileName = "/xx-sched.csv";
    private static Set<Flight> allFlights = new HashSet<>();

    public Set<Flight> getAllFlights() {
        if(allFlights.size()==0 || allFlights==null)
            aggregateFlightsFromCsv(fileName);
        return allFlights;
    }

    public void aggregateFlightsFromCsv(String fileName) {
        String line = "";
        String splitBy = ",";
        try
        {
            InputStream inputStream = getClass().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null)
            {
                String[] flights = line.split(splitBy);
                Flight flight = new Flight(flights[0],flights[2],flights[1],flights[3],flights[4]);
                allFlights.add(flight);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
