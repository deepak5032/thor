package com.mmt.thor.service;

import com.mmt.thor.entity.Flight;
import com.mmt.thor.entity.ResponseDto;
import org.springframework.stereotype.Service;


//build response objects
@Service
public class ResponseBuilder {

    public ResponseDto buildResponse(Flight flight,String source,String destination) {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setCityCodes(source+"_"+destination);
        responseDto.setFlightNos(flight.getFlightNo());
        int startTime = Integer.parseInt(flight.getStartTime());
        int endTime = Integer.parseInt(flight.getEndTime());
        responseDto.setJourneyTime(calculateDuration(startTime,endTime));
        return responseDto;
    }

    public ResponseDto buildResponse(Flight firstFlight,Flight secondFlight,String source,String destination) {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setCityCodes(source+"_"+firstFlight.getToAirport()+"-"+destination);
        responseDto.setFlightNos(firstFlight.getFlightNo()+"_"+secondFlight.getFlightNo());
        int startTime = Integer.parseInt(firstFlight.getStartTime());
        int endTime = Integer.parseInt(secondFlight.getEndTime());
        if(Integer.parseInt(secondFlight.getStartTime())>endTime)
            endTime+=2400;
        responseDto.setJourneyTime(calculateDuration(startTime,endTime));
        return responseDto;
    }

    public int calculateDuration(int startTime,int endTime) {
        int durationInMinutes=0;
        if(endTime>startTime)
            durationInMinutes = (endTime - startTime) / 100 * 60 + (endTime - startTime) % 100;
        else
            durationInMinutes = ((2400+endTime)-startTime)/100*60+((2400+endTime)-startTime)%100;
        return durationInMinutes;
    }

}
