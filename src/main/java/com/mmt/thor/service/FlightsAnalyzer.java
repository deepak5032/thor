package com.mmt.thor.service;

import com.mmt.thor.entity.Flight;
import com.mmt.thor.entity.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FlightsAnalyzer {

    @Autowired
    FlightAggregator flightAggregator;

    @Autowired
    ResponseBuilder responseBuilder;

    public List<List<ResponseDto>> getNWaysToTravel(int n, String source, String destination) {
        if(n==0)
            return new ArrayList<>();
        List<List<ResponseDto>> matchedFlights = new ArrayList<>();
        List<ResponseDto> directFlights = new ArrayList<>();
        List<ResponseDto> twoFlights = new ArrayList<>();
        Set<String> uniquePath = new HashSet<>();
        Set<Flight> allFlights = flightAggregator.getAllFlights();
        for (Flight flight : allFlights) {
            if (flight.getFromAirport().equalsIgnoreCase(source) && flight.getToAirport().equalsIgnoreCase(destination)) {
                directFlights.add(responseBuilder.buildResponse(flight, source, destination));
            } else if (flight.getToAirport().equalsIgnoreCase(destination)) {
                for (Flight connectingFlight : allFlights) {
                    if (connectingFlight.getToAirport().equalsIgnoreCase(flight.getFromAirport()) &&
                            connectingFlight.getFromAirport().equalsIgnoreCase(source)) {
                        if (Integer.parseInt(flight.getStartTime()) > Integer.parseInt(connectingFlight.getEndTime())) {
                            if (Integer.parseInt(flight.getStartTime()) - Integer.parseInt(connectingFlight.getEndTime()) / 60 > 2) {
                                ResponseDto responseDto = responseBuilder.buildResponse(connectingFlight, flight, source, destination);
                                if(uniquePath.contains(responseDto.getCityCodes())) {
                                    continue;
                                } else {
                                    twoFlights.add(responseDto);
                                    uniquePath.add(responseDto.getCityCodes());
                                }
                            }

                        }
                    }
                }
            }

        }
        Collections.sort(directFlights, new Comparator<ResponseDto>() {
            @Override
            public int compare(ResponseDto o1, ResponseDto o2) {
                return Integer.compare(o1.getJourneyTime(),o2.getJourneyTime());
            }
        });
        matchedFlights.add(directFlights);
        Collections.sort(twoFlights, new Comparator<ResponseDto>() {
            @Override
            public int compare(ResponseDto o1, ResponseDto o2) {
                return Integer.compare(o1.getJourneyTime(),o2.getJourneyTime());
            }
        });
        if(n>1)
            matchedFlights.add(twoFlights);
        return matchedFlights;
    }
}
